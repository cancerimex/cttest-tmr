$(function(){

    $('#add_product').on('click', function(e) {
        e.preventDefault();

        var form = $('form');
        var name = $('input[name=name]').val();
        var qty = $('input[name=qty]').val();
        var price = $('input[name=price]').val();

        if (name == '' || qty == '' || price == '') {
            alert('All fields are required');
            return false;
        }

        if ( qty % 1 !== 0) {
            alert('The Quantity in stock field must be a whole number');
            return false;
        }

        if (isNaN(price)) {
            alert("The Price field must be a number");
        }

        $.ajax({
            type: 'POST',
            url: 'product',
            dataType: 'json',
            cache: false,
            data: form.serialize(),
            success: function(data) {
                $('#noresults').remove();
                $('#product_table tbody').append(
                    '<tr><td>' + data.name + '</td><td>' + data.qty + '</td><td>' + data.price + '</td><td>' + data.created + '</td><td>' + data.total + '</td><td></td></tr>'
                );

                $('#totals').remove();
                var total = parseFloat(data.total);
                var subtotal = $('.sub-totals');
                $.each(subtotal, function(index, row) {
                    total = total + parseFloat($(row).text());
                });

                $('#product_table tbody').append(
                    '<tr id="totals"><td colspan="4" class="text-right"><b>Total:</b></td><td id="total">'+total.toFixed(2)+'</td><td></td></tr>'
                );
            }
        })
    });

});
//# sourceMappingURL=all.js.map
