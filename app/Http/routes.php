<?php

Route::get('/', 'ProductController@create')->name('product.create');
Route::post('/product', 'ProductController@store')->name('product.store');
Route::patch('/product', 'ProductController@update')->name('product.update');
Route::delete('/product', 'ProductController@delete')->name('product.delete');
