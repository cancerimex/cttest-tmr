<?php

namespace App\Http\Controllers;

use DateTimeZone;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (File::exists('json/products.json')) {
            $products = json_decode(file_get_contents('json/products.json'));
        } else {
            File::put('json/products.json', '');
        }

        return view('product.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');

        if ($request->ajax()) {
            $products = json_decode(file_get_contents('json/products.json'));

            $data['created'] = Carbon::now(new DateTimeZone('America/Los_Angeles'))->toDayDateTimeString();
            $data['total'] = number_format($data['qty'] * $data['price'],2);
            $data['price'] = number_format($data['price'], 2);

            $products->data[] =  $data;

            File::put('json/products.json', json_encode($products));

            return response()->json($data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // unable to get to this
    }

}
