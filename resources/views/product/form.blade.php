<!--- Product name Field --->
<div class="form-group">
    {!! Form::label('name', 'Product name:', ['class' => 'col-md-2']) !!}
    <div class="col-md-10">
        {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
    </div>
</div>

<!--- Quantity in stock Field --->
<div class="form-group">
    {!! Form::label('qty', 'Quantity in stock:', ['class' => 'col-md-2']) !!}
    <div class="col-md-10">
        {!! Form::text('qty', null, ['class' => 'form-control', 'required']) !!}
    </div>
</div>

<!--- Price per item Field --->
<div class="form-group">
    {!! Form::label('price', 'Price per item:', ['class' => 'col-md-2']) !!}
    <div class="col-md-10">
        {!! Form::text('price', null, ['class' => 'form-control', 'required']) !!}
    </div>
</div>

<!--- Submit Button Field --->
<div class="form-group">
    <div class="col-md-offset-2 col-md-10">
        {!! Form::submit('Add Product', ['class' => 'btn btn-primary', 'id' => 'add_product']) !!}
    </div>
</div>