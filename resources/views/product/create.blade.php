@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Products</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['route' => 'product.store', 'method' => 'post', 'class' => 'form-horizontal']) !!}

                @include('product.form')

                {!! Form::close() !!}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped" id="product_table">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Quantity in stock</th>
                        <th>Price per item</th>
                        <th>Datetime submitted</th>
                        <th>Total value number</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ( $products )
                        <?php $total = 0; ?>
                        @foreach ($products->data as $p)
                            <tr>
                                <td>{{ $p->name }}</td>
                                <td>{{ $p->qty }}</td>
                                <td>{{ $p->price }}</td>
                                <td>{{ $p->created }}</td>
                                <td class="sub-totals">{{ $p->total }}</td>
                                <td></td>
                            </tr>
                            <?php $total = $total + $p->total; ?>
                        @endforeach
                        <tr id="totals">
                            <td colspan="4" class="text-right"><b>Total:</b></td>
                            <td id="total">{{ number_format($total, 2) }}</td>
                            <td></td>
                        </tr>
                    @else
                        <tr id="noresults">
                            <td colspan="6" class="text-center">No Data Available</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection